package com.example.incivismedef;

public class Incidencia {
    String nom;
    String descripcio;
    String latitud;
    String longitud;
    String direccio;
    String url;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getDireccio() {
        return direccio;
    }

    public void setDireccio(String direccio) {
        this.direccio = direccio;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Incidencia(String nom, String descripcio, String latitud, String longitud, String direccio, String url) {
        this.nom = nom;
        this.descripcio = descripcio;
        this.latitud = latitud;
        this.longitud = longitud;
        this.direccio = direccio;
        this.url = url;
    }

    @Override
    public String toString() {
        return nom +": "+descripcio;
    }
    public Incidencia(){

    }
}
