package com.example.incivismedef.ui.notifications;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.incivismedef.AdapterMap;
import com.example.incivismedef.Incidencia;
import com.example.incivismedef.R;
import com.example.incivismedef.ui.home.HomeViewModel;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class NotificationsFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private DatabaseReference mDatabase;
    private ArrayList<Marker> tmpRealTimeMakers = new ArrayList<>();
    GoogleMap myGoogle;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_notifications, container, false);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map_google);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Incidencia");
        mapFragment.getMapAsync(map -> {
            // Codi a executar quan el mapa s'acabi de carregar.

            mDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    //fills node
                    for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                        Incidencia pro = snapshot.getValue(Incidencia.class);
                        AdapterMap customInfoWindow = new AdapterMap(
                                getActivity()
                        );
                        Double lati = Double.valueOf(pro.getLatitud());
                        Double longi = Double.valueOf(pro.getLongitud());
                        Marker marker = map.addMarker(new MarkerOptions()
                                .title(pro.getNom())
                                .snippet(pro.getDescripcio())
                                .position(new LatLng(lati,longi)));
                        marker.setTag(pro);
                        map.setInfoWindowAdapter(customInfoWindow);

                    }


                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        });



        return root;
    }
}