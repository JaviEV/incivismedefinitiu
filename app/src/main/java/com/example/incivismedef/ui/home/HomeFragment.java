package com.example.incivismedef.ui.home;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.example.incivismedef.Incidencia;
import com.example.incivismedef.R;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private Button bLocation;
    private static final int REQUEST_LOCATION_PERMISION = 1;
    private TextView textLatLong,textAddress;
    private ProgressBar progressBar;
    private EditText mEditText,mdes;
    private DatabaseReference mDatabase;
    //----- storage
    private Button mUploadBtn;
    private StorageReference mStorage;
    private static final int GALLERY_INTENT = 1;
    private ImageView image;
    private ProgressDialog mProgressDialog;
    private static String url;



    public HomeFragment() {
        // Required empty public constructor
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        bLocation = root.findViewById(R.id.buttonGetCurrentLocation);
        textLatLong = root.findViewById(R.id.textLatLong);
        progressBar = root.findViewById(R.id.progressBar);
        textAddress = root.findViewById(R.id.textAddress);
        mEditText = root.findViewById(R.id.editText);
        mdes = root.findViewById(R.id.descripcioText);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        //----
        mUploadBtn = (Button)root.findViewById(R.id.botonFoto);
        mStorage = FirebaseStorage.getInstance().getReference();
        image = root.findViewById(R.id.foto);
        //--------
        mProgressDialog = new ProgressDialog(getContext());


        progressBar.setVisibility(View.GONE);

        mUploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent,GALLERY_INTENT);
            }
        });

        bLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check();

            }
        });

        return root;
    }
    private void check(){
        Log.i("Myactivity","entra");
        progressBar.setVisibility(View.VISIBLE);

        if (ContextCompat.checkSelfPermission(this.getContext(), Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED ){
            ActivityCompat.requestPermissions(this.getActivity(),new String[]{Manifest.permission.ACCESS_FINE_LOCATION},REQUEST_LOCATION_PERMISION);
            getCurrentLocation();

        }else {
            Log.i("Myactivity","no permisos");

        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i("Myactivity","entra2");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION_PERMISION && grantResults.length > 0 ){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                getCurrentLocation();
            }else {
                Toast.makeText(this.getContext(), "Permission denied", Toast.LENGTH_SHORT).show();
            }

        }
    }
    private void getCurrentLocation(){
        Log.i("Myactivity","entra3");
        progressBar.setVisibility(View.VISIBLE);

        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(3000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationServices.getFusedLocationProviderClient(this.getContext())
                .requestLocationUpdates(locationRequest,new LocationCallback(){
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        super.onLocationResult(locationResult);
                        LocationServices.getFusedLocationProviderClient(getActivity()).removeLocationUpdates(this);
                        if (locationResult != null && locationResult.getLocations().size() > 0){
                            int lastestLocationIndex = locationResult.getLocations().size() - 1;
                            double latitude = locationResult.getLocations().get(lastestLocationIndex).getLatitude();
                            double longitude = locationResult.getLocations().get(lastestLocationIndex).getLongitude();
                            textLatLong.setText(
                                    String.format(
                                            "Latitude: %s\nLongitude: %s",
                                            latitude,
                                            longitude
                                    )
                            );

                            Location location = new Location("providerNA");
                            location.setLatitude(latitude);
                            location.setLongitude(longitude);
                            String cityname = getCityNAme(location);
                            textAddress.setText(cityname);
                            campos(latitude,longitude,cityname);

                        }progressBar.setVisibility(View.GONE);

                    }
                }, Looper.getMainLooper());
    }
    private void campos(double la , double lo, String li){
        String nom = mEditText.getText().toString();
        String desc = mdes.getText().toString();
        String lati = String.valueOf(la);
        String longi = String.valueOf(lo);
        Incidencia in = new Incidencia(nom,desc,lati,longi,li,url);
        mDatabase.child("Incidencia").push().setValue(in);

    }
    private String getCityNAme(Location lo){
        String city = "";
        Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lo.getLatitude(),lo.getLongitude(),1);
            String address = addresses.get(0).getAddressLine(0);
            city = addresses.get(0).getLocality();
            city = address;

            Log.d("mylog","complete"+addresses.toString());
            Log.d("mylog","complete"+address);


        } catch (IOException e) {
            e.printStackTrace();
        }
        return city;


    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_INTENT && resultCode == getActivity().RESULT_OK){
            mProgressDialog.setTitle("pujant foto");
            mProgressDialog.setMessage("Pujant foto a firebase");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            Uri uri = data.getData();
            StorageReference filePath = mStorage.child("fotoss").child(uri.getLastPathSegment());
            UploadTask uploadTask = filePath.putFile(uri);
            uploadTask.addOnSuccessListener(taskSnapshot -> {
                taskSnapshot.getMetadata();
                filePath.getDownloadUrl().addOnCompleteListener(task -> {
                    mProgressDialog.dismiss();
                    Uri dowlo =task.getResult();
                    Glide.with(getContext()).load(dowlo).into(image);

                    url = dowlo.toString();

                });

            });

        }

    }

}