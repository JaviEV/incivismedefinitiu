package com.example.incivismedef.ui.dashboard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.incivismedef.Incidencia;
import com.example.incivismedef.R;
import com.example.incivismedef.ui.home.HomeViewModel;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class DashboardFragment extends Fragment {

    private HomeViewModel homeViewModel;

    private List<Incidencia> listInciden = new ArrayList<Incidencia>();
    ArrayAdapter<Incidencia> arrayAdaperInci;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    ListView listInci;

    public DashboardFragment() {
        // Required empty public constructor
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);
        listInci = root.findViewById(R.id.lvInciden);

        iniciarFirebase();

        listarDatos();


        return root;
    }

    private void listarDatos() {
        databaseReference.child("Incidencia").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listInciden.clear();
                for (DataSnapshot objSnapshot : dataSnapshot.getChildren()){
                    Incidencia p = objSnapshot.getValue(Incidencia.class);
                    if (getActivity() != null){
                        listInciden.add(p);
                        arrayAdaperInci = new ArrayAdapter<Incidencia>(getContext(),android.R.layout.simple_list_item_1,listInciden);
                        listInci.setAdapter(arrayAdaperInci);
                    }

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private void iniciarFirebase() {
        FirebaseApp.initializeApp(getContext());
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
    }
}