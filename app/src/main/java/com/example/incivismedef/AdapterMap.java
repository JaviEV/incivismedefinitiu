package com.example.incivismedef;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class AdapterMap implements GoogleMap.InfoWindowAdapter{

    private final Activity activity;

    public AdapterMap(Activity activity){
        this.activity = activity;

    }
    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = activity.getLayoutInflater().inflate(R.layout.info_point_map,null);

        Incidencia in = (Incidencia) marker.getTag();

        ImageView imageView = view.findViewById(R.id.imageL);
        TextView tvTitle = view.findViewById(R.id.title);
        TextView tvDes = view.findViewById(R.id.description);

        tvTitle.setText(in.getNom());
        tvDes.setText(in.getDescripcio());
        Glide.with(activity).load(in.getUrl()).into(imageView);

        return view;
    }



}
